package org.spaxe.airsup.screens;

import org.spaxe.airsup.gameplay.TestGameplay;

/**
 * Created by Michel on 24-5-2015.
 */
public class TestGameScreen extends GameScreen {
    @Override
    public void show() {
        super.show();
        gameplay = new TestGameplay();
    }

}
