package org.spaxe.airsup.screens;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import org.spaxe.airsup.Math.MathUtil;
import org.spaxe.airsup.agents.Agent;

/**
 * Created by Michel on 24-5-2015.
 */
public class BezierTestGameScreen extends TestGameScreen
{
    private boolean pressed = false;
    private boolean selected = false;
    private float startX;
    private float startY;
    private float currentX;
    private float currentY;
    private Agent selectedAgent;
    private ShapeRenderer shapeRenderer;

    @Override
    public void show() {
        super.show();
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if(!selected)
        {
            if (!pressed) {
            Vector3 vect = GetScreenToWorld(screenX, screenY);
                selectedAgent = gameplay.select(vect.x, vect.y);
            if(selectedAgent != null) {

                    pressed = true;

                    startX = selectedAgent.getX()+(selectedAgent.getWidth()/2);
                    startY = selectedAgent.getY()+(selectedAgent.getHeight()/2);
                    currentX = vect.x;
                    currentY = vect.y;


            }
            } else if(selectedAgent != null){
                System.out.println(MathUtil.getAngle(startX, startY, currentX, currentY));
                selectedAgent.setRotation(MathUtil.getAngle(startX, startY, currentX, currentY));
                selectedAgent.setCenter(currentX, currentY);
                pressed = false;
            }
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        super.mouseMoved(screenX, screenY);
        if(pressed)
        {
            Vector3 vect = GetScreenToWorld(screenX, screenY);
            if(selectedAgent != null) {
                Vector2 movement = selectedAgent.getMovement(startX, startY, vect.x, vect.y);
                int newAngle = selectedAgent.getAngle(MathUtil.getAngle(startX, startY, vect.x, vect.y));
                if(newAngle <= selectedAgent.getMaxAngle()) {
                    currentX = movement.x;
                    currentY = movement.y;
                }
            }
        }
        return false;
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if(pressed) {
            shapeRenderer.setProjectionMatrix(camera.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(0.5f, 0.5f, 0.4f, 0.1f);
            shapeRenderer.rectLine(startX, startY, currentX, currentY, 5);

            shapeRenderer.end();
        }


    }
}
