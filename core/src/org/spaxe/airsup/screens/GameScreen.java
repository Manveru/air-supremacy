package org.spaxe.airsup.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.HexagonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import org.spaxe.airsup.agents.Agent;
import org.spaxe.airsup.gameplay.Gameplay;

/**
 * Created by Michel on 21-5-2015.
 */
public class GameScreen implements Screen, InputProcessor {
    private TiledMap map;
    private HexagonalTiledMapRenderer renderer;
    protected OrthographicCamera camera;
    //float unitScale = 2f;
    protected Gameplay gameplay;

    @Override
    public void show() {
        TmxMapLoader loader = new TmxMapLoader();
        // java: 94 x 37
        // Tiles: 90 x 55
        map = loader.load("maps/test.tmx");

        renderer = new HexagonalTiledMapRenderer(map);
        camera = new OrthographicCamera();


        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.setView(camera);
        renderer.render();

        renderer.getBatch().begin();
        for(Agent agent : gameplay.agents)
        {
            agent.draw(renderer.getBatch());
        }
        renderer.getBatch().end();

        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            camera.translate(-10, 0);
            camera.update();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            camera.translate(10, 0);
            camera.update();
        }

        if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            camera.translate(0, 10);
            camera.update();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            camera.translate(0, -10);
            camera.update();
        }

    }

    public Vector3 GetScreenToWorld(int x, int y)
    {
        Vector3 pos = new Vector3(x, y, 0);
        return camera.unproject(pos);
    }

    public Vector3 pixelToHex(float x, float y) {

        float q = (x * (float)Math.sqrt(3) / 3 - y / 3) / 37;
        float r = y * 2 / 3 / 94;
        return new Vector3(q, r, 0);
    }

    public Vector3 hexToCube(Vector3 vect) {
        float x = vect.x;
        float z = vect.y;
        float y = -x - z;
        return new Vector3(x, y, z);
    }

    public Vector3 cubeRound(Vector3 h) {
        float rx = Math.round(h.x);
        float ry = Math.round(h.y);
        float rz = Math.round(h.z);

        float x_diff = Math.abs(rx - h.x);
        float y_diff = Math.abs(ry - h.y);
        float z_diff = Math.abs(rz - h.z);

        if (x_diff > y_diff && x_diff>z_diff) {
            rx = -ry - rz;
        }
        else if( y_diff > z_diff) {
            ry = -rx - rz;
        }
        else {
            rz = -rx - ry;
        }

        return new Vector3(rx, ry, rz);
    }


    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {


        System.out.println(button + " " + screenX + " " + screenY );
        Vector3 vect = GetScreenToWorld(screenX, screenY);

        System.out.println(button + " " + vect.x + " " + vect.y );

        Vector3 hex = pixelToHex(vect.x, vect.y);
        Vector3 round = cubeRound(hexToCube(hex));
        System.out.println(button + " " + round.x + " " + round.y );
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
