package org.spaxe.airsup.agents;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Michel on 22-5-2015.
 */
public class Agent extends Sprite{

    protected String name;
    protected int year;
    protected String manoeuvre;
    protected float currentSpeed;

    public Agent(String name, int year, String sprite, String manoeuvre)
    {
        super(new Sprite(new Texture(sprite)));
        setOriginCenter();
        this.name = name;
        this.year = year;
        this.manoeuvre = manoeuvre;
    }


    public Vector2 getMovement(float x, float y, float x2, float y2)
    {
        float dirX = x2 - x;
        float dirY = y2 - y;

        float dirLen = (float) Math.sqrt(dirX * dirX + dirY * dirY); // The length of dir

        dirX = dirX / dirLen;
        dirY = dirY / dirLen;

        float length = (currentSpeed / 2) *25;

        float lineX = (dirX * length) + x;
        float lineY = (dirY * length) + y;
    
        return new Vector2(lineX, lineY);
    }

    public Agent(Sprite sprite)
    {
        super(sprite);
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getAngle(float mouseAngle)
    {
        int d = (int) (Math.abs(this.getRotation() - mouseAngle) % 360);
        int r = d > 180 ? 360 - d : d;
        return r;
    }

    public float getMaxAngle() {
        switch (manoeuvre)
        {
            case "L":
                return 15;
            case "M":
                return 30;
            case "H":
                return 45;
        }
        return 0;
    }
}
