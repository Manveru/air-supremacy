package org.spaxe.airsup.agents;

import org.spaxe.airsup.xml.dto.AircraftDto;

/**
 * Created by Michel on 22-5-2015.
 */
public class Airplane extends Agent
{
    public Airplane(AircraftDto dto) {
        super(dto.getName(), dto.getYear(), dto.getIcon(), dto.getMa());
    }
}
