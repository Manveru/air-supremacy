package org.spaxe.airsup.agents.factories;

import org.spaxe.airsup.agents.Airplane;
import org.spaxe.airsup.xml.dto.AircraftDto;

import java.util.ArrayList;

/**
 * Created by Michel on 24-5-2015.
 */
public class AirplaneFactory extends AgentFactory
{
    public static ArrayList<Airplane> getAirplanes(ArrayList<AircraftDto> dtos)
    {
        ArrayList<Airplane> airplanes = new ArrayList<>();
        for(AircraftDto dto : dtos)
        {
            System.out.println(dto.toString());
            Airplane airplane = new Airplane(dto);

            airplanes.add(airplane);

        }

        return airplanes;

    }
}
