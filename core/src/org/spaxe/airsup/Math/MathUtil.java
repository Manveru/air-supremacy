package org.spaxe.airsup.Math;

/**
 * Created by Michel on 24-5-2015.
 */
public class MathUtil {

    public static float getAngle(float x1, float y1, float x2, float y2)
    {
        float deltaY = y2 - y1;
        float deltaX = x2 - x1;
        float radials = (float)Math.atan2(deltaY, deltaX);
        return (float) ((radials > 0 ? radials : (2*Math.PI + radials)) * 360 / (2*Math.PI));

    }
}
