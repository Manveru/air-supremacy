package org.spaxe.airsup.xml;


import org.spaxe.airsup.xml.dto.AirToAirMissleDto;
import org.spaxe.airsup.xml.dto.AircraftDto;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Michel on 24-5-2015.
 */
public class AssetFactory {
    public static AircraftDto jaxbXMLToAircraft(File file) {

        try {
            JAXBContext context = JAXBContext.newInstance(AircraftDto.class);
            Unmarshaller un = context.createUnmarshaller();
            AircraftDto emp = (AircraftDto) un.unmarshal(file);
            return emp;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<AircraftDto> getAircraft() {
        File file = new File("aircraft/");
        ArrayList<AircraftDto> aircrafts = new ArrayList<>();
        File[] files = file.listFiles();
        if (files != null) {
            for (File aircraftFile: files) {
                if (aircraftFile != null) {
                    AircraftDto aircraft = jaxbXMLToAircraft(aircraftFile);
                    if(aircraft != null)
                    {
                        aircrafts.add(aircraft);
                    }
                }
            }
        }
        return aircrafts;
    }

    public static AirToAirMissleDto jaxbXMLToAirToAirMissle(File file) {

        try {
            JAXBContext context = JAXBContext.newInstance(AirToAirMissleDto.class);
            Unmarshaller un = context.createUnmarshaller();
            AirToAirMissleDto emp = (AirToAirMissleDto) un.unmarshal(file);
            return emp;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<AirToAirMissleDto> getAirToAirMissle() {
        File file = new File("airtoair/");
        ArrayList<AirToAirMissleDto> aircrafts = new ArrayList<>();
        File[] files = file.listFiles();
        if (files != null) {
            for (File aircraftFile: files) {
                if (aircraftFile != null) {
                    AirToAirMissleDto aircraft = jaxbXMLToAirToAirMissle(aircraftFile);
                    if(aircraft != null)
                    {
                        aircrafts.add(aircraft);
                    }
                }
            }
        }
        return aircrafts;
    }

}
