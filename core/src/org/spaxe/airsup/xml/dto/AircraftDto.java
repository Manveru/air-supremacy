package org.spaxe.airsup.xml.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Michel on 24-5-2015.
 */
@XmlRootElement(name = "Aircraft")
@XmlType(propOrder = {"name", "mn", "mx", "pw", "acc1", "acc2", "dec", "ma", "gn", "am", "bm", "dp", "cf", "fl", "ecm", "rdr", "rdrl", "sz", "sp", "ld", "year", "load", "notes", "icon"})
public class AircraftDto {

    private int id;
    private String name;
    private int mn;
    private int mx;
    private String pw;
    private int acc1;
    private int acc2;
    private int dec;
    private String ma;
    private int gn;
    private int am;
    private int bm;
    private int dp;
    private int cf;
    private int fl;
    private int ecm;
    private int rdr;
    private int rdrl;
    private int sz;
    private int sp;
    private int ld;
    private int year;
    private String load;
    private String notes;
    private String icon;

    @XmlAttribute
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    @XmlElement (name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement (name = "mn")
    public int getMn() {
        return mn;
    }

    public void setMn(int mn) {
        this.mn = mn;
    }

    @XmlElement (name = "mx")
    public int getMx() {
        return mx;
    }

    public void setMx(int mx) {
        this.mx = mx;
    }

    @XmlElement (name = "pw")
    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    @XmlElement (name = "acc1")
    public int getAcc1() {
        return acc1;
    }

    public void setAcc1(int acc1) {
        this.acc1 = acc1;
    }

    @XmlElement (name = "acc2")
    public int getAcc2() {
        return acc2;
    }

    public void setAcc2(int acc2) {
        this.acc2 = acc2;
    }

    @XmlElement (name = "dec")
    public int getDec() {
        return dec;
    }

    public void setDec(int dec) {
        this.dec = dec;
    }

    @XmlElement (name = "ma")
    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }
    @XmlElement (name = "gn")
    public int getGn() {
        return gn;
    }

    public void setGn(int gn) {
        this.gn = gn;
    }
    @XmlElement (name = "am")
    public int getAm() {
        return am;
    }

    public void setAm(int am) {
        this.am = am;
    }
    @XmlElement (name = "bm")
    public int getBm() {
        return bm;
    }

    public void setBm(int bm) {
        this.bm = bm;
    }
    @XmlElement (name = "dp")
    public int getDp() {
        return dp;
    }

    public void setDp(int dp) {
        this.dp = dp;
    }
    @XmlElement (name = "cf")
    public int getCf() {
        return cf;
    }

    public void setCf(int cf) {
        this.cf = cf;
    }
    @XmlElement (name = "fl")
    public int getFl() {
        return fl;
    }

    public void setFl(int fl) {
        this.fl = fl;
    }
    @XmlElement (name = "ecm")
    public int getEcm() {
        return ecm;
    }

    public void setEcm(int ecm) {
        this.ecm = ecm;
    }
    @XmlElement (name = "rdr")
    public int getRdr() {
        return rdr;
    }

    public void setRdr(int rdr) {
        this.rdr = rdr;
    }
    @XmlElement (name = "rdrl")
    public int getRdrl() {
        return rdrl;
    }

    public void setRdrl(int rdrl) {
        this.rdrl = rdrl;
    }
    @XmlElement (name = "sz")
    public int getSz() {
        return sz;
    }

    public void setSz(int sz) {
        this.sz = sz;
    }

    @XmlElement (name = "sp")
    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }
    @XmlElement (name = "ld")
    public int getLd() {
        return ld;
    }

    public void setLd(int ld) {
        this.ld = ld;
    }
    @XmlElement (name = "year")
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    @XmlElement (name = "load")
    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }
    @XmlElement (name = "notes")
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @XmlElement (name = "icon")
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "ID = " + id + " NAME=" + name + " YEAR=" + year + " NOTES=" + notes+ " ICON=" + icon;
    }

}

