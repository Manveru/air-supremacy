package org.spaxe.airsup.xml.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Michel on 24-5-2015.
 */
@XmlRootElement(name = "AirToAirMissle")
@XmlType(propOrder = {"name", "guidance", "aspect", "lock", "min", "max", "hit", "damage", "load", "year"})
public class AirToAirMissleDto {

    private int id;
    private String name;
    private String guidance;
    private String aspect;
    private String lock;
    private int min;
    private int max;
    private String hit;
    private String damage;
    private float load;
    private int year;

    @XmlAttribute
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement (name = "guidance")
    public String getGuidance() {
        return guidance;
    }

    public void setGuidance(String guidance) {
        this.guidance = guidance;
    }
    @XmlElement (name = "aspect")
    public String getAspect() {
        return aspect;
    }

    public void setAspect(String aspect) {
        this.aspect = aspect;
    }
    @XmlElement (name = "lock")
    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }
    @XmlElement (name = "min")
    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
    @XmlElement (name = "max")
    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
    @XmlElement (name = "hit")
    public String getHit() {
        return hit;
    }

    public void setHit(String hit) {
        this.hit = hit;
    }
    @XmlElement (name = "damage")
    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }
    @XmlElement (name = "load")
    public float getLoad() {
        return load;
    }

    public void setLoad(float load) {
        this.load = load;
    }
    @XmlElement (name = "year")
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
