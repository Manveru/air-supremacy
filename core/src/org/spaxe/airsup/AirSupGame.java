package org.spaxe.airsup;

import com.badlogic.gdx.Game;
import org.spaxe.airsup.screens.BezierTestGameScreen;

public class AirSupGame extends Game {
    @Override
    public void create () {
        setScreen(new BezierTestGameScreen());
    }
}
