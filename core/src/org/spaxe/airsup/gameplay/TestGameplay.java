package org.spaxe.airsup.gameplay;

import org.spaxe.airsup.agents.Airplane;

/**
 * Created by Michel on 24-5-2015.
 */
public class TestGameplay extends Gameplay
{
    public enum Phase {
      MOVE1, TURN1, MOVE2, TURN2
    }

    public Phase CurrentPhase = Phase.MOVE1;

    public TestGameplay()
    {
        super();

        Airplane f14 = new Airplane(airplanes.get(0));
        f14.setCurrentSpeed(12);
        agents.add(f14);
    }

}
