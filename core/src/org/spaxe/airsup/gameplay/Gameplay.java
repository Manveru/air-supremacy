package org.spaxe.airsup.gameplay;

import org.spaxe.airsup.agents.Agent;
import org.spaxe.airsup.xml.AssetFactory;
import org.spaxe.airsup.xml.dto.AirToAirMissleDto;
import org.spaxe.airsup.xml.dto.AircraftDto;

import java.util.ArrayList;

/**
 * Created by Michel on 24-5-2015.
 */
public class Gameplay
{
    public ArrayList<Agent> agents = new ArrayList<>();
    public ArrayList<AircraftDto> airplanes = new ArrayList<>();
    public ArrayList<AirToAirMissleDto> airToAirMissles = new ArrayList<>();

    public Gameplay()
    {
        InitWeapons();
        InitAircraft();
    }

    public Agent select(float x, float y)
    {
        for (Agent agent : agents) {
            if (x > agent.getX() && x < agent.getX() + agent.getWidth()) {
                if (y > agent.getY() && y < agent.getY() + agent.getHeight()) {
                    return agent;
                }
            }

        }
        return null;
    }

    private void InitAircraft() {
        airplanes = AssetFactory.getAircraft();
    }

    private void InitWeapons() {
        airToAirMissles = AssetFactory.getAirToAirMissle();
    }
}
